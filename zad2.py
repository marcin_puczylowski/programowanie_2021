student_input_1 = input("Wiek pierwszego studenta: ")
student_input_2 = 0
with open("wiek_drugiego_studenta.txt", "r") as plik:
        student_input_2 = plik.read()

wiek_1 = int(student_input_1)
wiek_2 = int(student_input_2)

if wiek_1 < wiek_2:
    roznica = str(wiek_2 - wiek_1)
    wynik = "Pierwszy student jest młodszy od studenta drugiego o " + roznica + " lat(a)."
    print(wynik)
    with open("wiek2.txt", "a") as plik:
        plik.write(wynik)
